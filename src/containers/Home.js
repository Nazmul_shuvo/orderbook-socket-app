import { useState } from "react";
import { Typography, Grid } from "@mui/material";
import Menu from "../Layouts/Menu";
import Tables from "../Layouts/Tables";

const Home = (props) => {
  const { product, buyList, sellList, handleChangeProduct } = props;
  const [showBuyTable, setShowBuyTable] = useState(true);
  const [showSellTable, setShowSellTable] = useState(true);
  const [activeTableView, setActiveTableView] = useState("Both");

  return (
    <Grid container spacing={4}>
      {/* Options to update data table with app title */}
      <Grid item sm>
        <Typography
          variant="h3"
          sx={{
            textAlign: "center",
            pb: 2,
            fontWeight: 600,
            color: "text.secondary",
          }}
        >
          Order Book
        </Typography>

        <Menu
          {...{
            activeTableView,
            setActiveTableView,
            setShowBuyTable,
            setShowSellTable,
            product,
            handleChangeProduct,
          }}
        />
      </Grid>

      {/* Data table */}
      <Grid item xs sx={{ p: 1 }}>
        <Tables
          {...{
            showBuyTable,
            showSellTable,
            buyList,
            sellList,
          }}
        />
      </Grid>
    </Grid>
  );
};

export default Home;
