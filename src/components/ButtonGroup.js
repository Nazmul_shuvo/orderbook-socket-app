import * as React from "react";
import Button from "@mui/material/Button";
import ButtonGroup from "@mui/material/ButtonGroup";

export default function BasicButtonGroup({ buttons = [], activeItem }) {
  return (
    <ButtonGroup aria-label="outlined primary button group">
      {buttons.map((button, i) => {
        return (
          <Button
            onClick={button.onClick ? button.onClick : (_) => {}}
            key={i}
            variant={activeItem === button.name ? "contained" : "outlined"}
          >
            {button.name}
          </Button>
        );
      })}
    </ButtonGroup>
  );
}
