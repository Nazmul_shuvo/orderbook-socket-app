import { useRef, useEffect } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { styled, makeStyles } from "@mui/styles";
import Tooltip from "./Tooltip";
import { green, grey, red } from "@mui/material/colors";

const useStyles = makeStyles((theme) => ({
  "@keyframes redBlinker": {
    from: {
      backgroundColor: "#fff",
      boxShadow: `inset 0px 0px 0px 6px ${red[50]}`,
    },
    to: {
      backgroundColor: red[100],
    },
  },
  blinkingRed: {
    animationName: "$redBlinker",
    animationDuration: "0.5s",
    animationTimingFunction: "linear",
    animationIterationCount: "1",
  },
  "@keyframes greenBlinker": {
    from: {
      backgroundColor: "#fff",
      boxShadow: `inset 0px 0px 0px 6px ${green[50]}`,
    },
    to: {
      backgroundColor: green[100],
    },
  },
  blinkingGreen: {
    animationName: "$greenBlinker",
    animationDuration: "0.5s",
    animationTimingFunction: "linear",
    animationIterationCount: "1",
  },
}));

const CustomTableRow = styled(TableRow)({
  border: `1px solid #ccc`,
});

const CustomTableCell = styled(TableCell)(({ theme, color }) => ({
  padding: "6px",
  fontSize: "14px",
  fontWeight: "400",
  width: "150px",
  color: color,
}));

const CustomTable = (props) => {
  const {
    data,
    heads,
    minWidth = 300,
    hoverScreenPosition = "left",
    hoverScreenParameters = [],
    startFromBottom = false,
  } = props;

  const classes = useStyles();
  const autoScrollDown = useRef();

  // it will start the table from bottom if startFromBottom is positive for any table
  useEffect(() => {
    if (autoScrollDown.current) {
      autoScrollDown.current.scrollIntoView({
        behavior: "auto",
        block: "end",
      });
    }
  }, []);

  const DEFAULT_TEXT_COLOR = grey[600];

  return (
    <TableContainer component={Paper} sx={{ height: "400px" }}>
      <Table sx={{ minWidth: minWidth }} aria-label="simple table">
        <TableBody ref={startFromBottom ? autoScrollDown : null}>
          {data.map((row, i) => {
            return (
              <Tooltip
                data={row}
                parameters={hoverScreenParameters}
                placement={hoverScreenPosition}
                key={i}
              >
                <CustomTableRow
                  sx={{
                    "&:last-child td, &:last-child th": { border: 0 },
                  }}
                >
                  {heads.map((item, i) => {
                    return (
                      <CustomTableCell
                        key={i}
                        component="th"
                        scope="row"
                        className={
                          item.accessor === "size"
                            ? row[item.comparison] === "inc"
                              ? classes.blinkingGreen
                              : row[item.comparison] === "dec"
                              ? classes.blinkingRed
                              : ""
                            : ""
                        }
                        color={
                          item.accessor === "limit_price"
                            ? row[item.type] === "buy"
                              ? green[500]
                              : row[item.type] === "sell"
                              ? red[500]
                              : DEFAULT_TEXT_COLOR
                            : DEFAULT_TEXT_COLOR
                        }
                        align={"center"}
                      >
                        {row[item.accessor]}
                      </CustomTableCell>
                    );
                  })}
                </CustomTableRow>
              </Tooltip>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default CustomTable;
