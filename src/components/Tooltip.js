import Tooltip from "@mui/material/Tooltip";
import { Box } from "@mui/system";

// This is a component to show on hover of any element
const HoverScreenComponent = (props) => {
  const { data, parameters } = props;
  return (
    <>
      {data && (
        <>
          {parameters &&
            parameters.length &&
            parameters.map((param, j) => {
              return (
                <Box sx={{ fontSize: "14px", py: 0.5 }} key={j}>
                  {param.name}: {data[param.accessor].toFixed(2)}
                </Box>
              );
            })}
        </>
      )}
    </>
  );
};

export default function BasicTooltip({
  children,
  data = [],
  placement = "left",
  parameters,
}) {
  return (
    <Tooltip
      title={
        <div>
          {typeof data === "string" || typeof data === "number" ? (
            data
          ) : (
            <HoverScreenComponent {...{ data, parameters }} />
          )}
        </div>
      }
      arrow={true}
      placement={placement}
    >
      {children}
    </Tooltip>
  );
}
