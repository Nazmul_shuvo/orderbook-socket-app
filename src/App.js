import { useEffect, useRef, useState } from "react";
import { Grid } from "@mui/material";
import { ProductOptions } from "./config/orderTable";
import Home from "./containers/Home";
import {
  dataWithCumulativeAvg,
  comparedNewData,
} from "./utils/dataModification";

const App = () => {
  const [product, setProduct] = useState(ProductOptions[0].value);
  const [buyList, setBuyList] = useState([]);
  const [sellList, setSellList] = useState([]);

  // websocket reference
  const ws = useRef(null);

  // subscribing to websocket and setting data to local store after proper modification
  useEffect(() => {
    ws.current = new WebSocket("wss://production-esocket.delta.exchange");
    ws.current.onopen = function () {
      const msg = {
        type: "subscribe",
        payload: {
          channels: [
            {
              name: "l2_orderbook",
              symbols: [product],
            },
          ],
        },
      };
      ws.current.send(JSON.stringify(msg));
    };
    ws.current.onmessage = function (msg) {
      const data = JSON.parse(msg.data);
      let buyDataWithAvg = dataWithCumulativeAvg(data.buy, "buy");
      let sellDataWithAvg = dataWithCumulativeAvg(data.sell, "sell", true);
      setBuyList((prev) => comparedNewData(prev, buyDataWithAvg));
      setSellList((prev) => comparedNewData(prev, sellDataWithAvg));
    };
  }, [product]);

  // on product change unsubscribe old subscription and setting the current product name
  const handleChangeProduct = (name) => {
    ws.current.onopen = function () {
      const msg = {
        type: "unsubscribe",
        payload: {
          channels: [
            {
              name: "l2_orderbook",
              symbols: [product],
            },
          ],
        },
      };
      ws.current.send(JSON.stringify(msg));
    };
    ws.current.close();
    ws.current = null;
    setProduct(name);
  };

  return (
    <Grid
      container
      justifyContent={"center"}
      alignItems={"center"}
      sx={{ p: 2 }}
    >
      <Grid item>
        <Home {...{ product, buyList, sellList, handleChangeProduct }} />
      </Grid>
    </Grid>
  );
};

export default App;
