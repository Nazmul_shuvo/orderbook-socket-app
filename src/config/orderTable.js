export const OrderTableColumns = [
  {
    name: "Price",
    accessor: "limit_price",
    type: "type",
    comparison: "comparison",
  },
  {
    name: "Quantity",
    accessor: "size",
    type: "type",
    comparison: "comparison",
  },
];

export const ProductOptions = [
  { name: "BTCUSDT", value: "BTCUSDT" },
  { name: "BTCUSD", value: "BTCUSD" },
  { name: "ETHUSDT", value: "ETHUSDT" },
  { name: "DOGEUSDT", value: "DOGEUSDT" },
  { name: "SOLUSDT", value: "SOLUSDT" },
];

export const HoverScreenParameters = [
  { name: "Total Price", accessor: "total_limit_price" },
  { name: "Average Price", accessor: "avg_limit_price" },
];
