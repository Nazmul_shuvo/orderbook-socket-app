import CustomTable from "../components/Table";
import Table from "@mui/material/Table";
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import TableHead from "@mui/material/TableHead";
import { Grid } from "@mui/material";
import { OrderTableColumns, HoverScreenParameters } from "../config/orderTable";

const Tables = (props) => {
  const { showBuyTable, showSellTable, buyList, sellList } = props;

  return (
    <Grid container justifyContent={"center"} direction="column">
      {/* Table Header */}
      {showBuyTable || showSellTable ? (
        <Grid item sx={{ pb: 0.5 }}>
          <Table sx={{ minWidth: 300 }} aria-label="simple table">
            <TableHead>
              <TableRow
                sx={{
                  border: `1px solid #ccc`,
                  backgroundColor: "primary.main",
                }}
              >
                {OrderTableColumns.map((column, i) => {
                  return (
                    <TableCell
                      key={i}
                      align="center"
                      sx={{
                        padding: "6px",
                        fontSize: "18px",
                        fontWeight: "400",
                        color: "primary.contrastText",
                      }}
                    >
                      {column.name}
                    </TableCell>
                  );
                })}
              </TableRow>
            </TableHead>
          </Table>
        </Grid>
      ) : (
        ""
      )}

      {/* Sell data table if Sell or Both is selected */}
      {showSellTable ? (
        <Grid item sx={{ pb: 0.5 }}>
          {sellList && sellList.length ? (
            <CustomTable
              data={sellList}
              heads={OrderTableColumns}
              hoverScreenPosition={"right"}
              hoverScreenParameters={HoverScreenParameters}
              startFromBottom={true}
            />
          ) : (
            ""
          )}
        </Grid>
      ) : (
        ""
      )}

      {/* Buy data table if Buy or Both is selected */}
      {showBuyTable ? (
        <Grid item>
          {buyList && buyList.length ? (
            <CustomTable
              data={buyList}
              heads={OrderTableColumns}
              hoverScreenPosition={"left"}
              hoverScreenParameters={HoverScreenParameters}
              showTableHead={false}
            />
          ) : (
            ""
          )}
        </Grid>
      ) : (
        ""
      )}
    </Grid>
  );
};

export default Tables;
