import Dropdown from "../components/Dropdown";
import ButtonGroup from "../components/ButtonGroup";
import { Grid } from "@mui/material";
import { ProductOptions } from "../config/orderTable";
import { MenuItems } from "../config/menu";

const Menu = (props) => {
  const {
    activeTableView,
    setActiveTableView,
    setShowBuyTable,
    setShowSellTable,
    product,
    handleChangeProduct,
  } = props;

  return (
    <Grid
      container
      sx={{ py: 2, minWidth: "300px" }}
      direction="column"
      spacing={4}
    >
      <Grid item sm>
        {/* Switches for Buy, Sell or Both data table */}
        <ButtonGroup
          activeItem={activeTableView}
          buttons={[
            {
              name: MenuItems[0].value,
              onClick: () => {
                setShowBuyTable(true);
                setShowSellTable(true);
                setActiveTableView(MenuItems[0].value);
              },
            },
            {
              name: MenuItems[1].value,
              onClick: () => {
                setShowBuyTable(true);
                setShowSellTable(false);
                setActiveTableView(MenuItems[1].value);
              },
            },
            {
              name: MenuItems[2].value,
              onClick: () => {
                setShowSellTable(true);
                setShowBuyTable(false);
                setActiveTableView(MenuItems[2].value);
              },
            },
          ]}
        />
      </Grid>
      <Grid item sm>
        {/* Product selection dropdown */}
        <Dropdown
          options={ProductOptions}
          value={product}
          setValue={handleChangeProduct}
          label={"Products"}
        />
      </Grid>
    </Grid>
  );
};

export default Menu;
