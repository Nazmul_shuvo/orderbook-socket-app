/**
 *
 * @param {array} data the data we want to modify
 * @param {string} dataType either 'buy' or 'sell'
 * @param {boolean} reverse if we want to reverse the data
 * @param {string} parameter depending on what parameter we want sum or average the data
 * @returns data with cumulative sum and average on every element
 */
export const dataWithCumulativeAvg = (
  data = [],
  dataType,
  reverse = false,
  parameter = "limit_price"
) => {
  const cumulatedData = [];
  let currentData = [];
  let sum = 0;

  if (reverse && data.length) {
    for (let i = data.length; i > 0; i--) {
      currentData.push(data[i - 1]);
    }
  } else {
    currentData = data;
  }

  for (let i in currentData) {
    i = parseInt(i);
    sum += parseFloat(currentData[i][parameter]);
    currentData[i][`total_${parameter}`] = sum;
    currentData[i][`avg_${parameter}`] = sum / (i + 1);
    currentData[i]["type"] = dataType;
    cumulatedData.push(currentData[i]);
  }

  return cumulatedData;
};

/**
 *
 * @param {array} oldData the data we want to compare with
 * @param {array} newData the data we want to compare to and return
 * @returns new data compared with every element if the weight is increased or decreased
 */
export const comparedNewData = (oldData, newData) => {
  let updatedData = newData;

  if (oldData.length) {
    for (let i = 0; i < oldData.length; i++) {
      if (oldData[i] && newData[i]) {
        if (oldData[i].size > newData[i].size) {
          updatedData[i].comparison = "dec";
        } else if (oldData[i].size < newData[i].size) {
          updatedData[i].comparison = "inc";
        } else updatedData[i].comparison = "eq";
      } else {
        if (updatedData[i]) updatedData[i].comparison = "eq";
      }
    }
  }
  return updatedData;
};
